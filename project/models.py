from project import db
from datetime import datetime
from sqlalchemy.ext.hybrid import hybrid_method, hybrid_property


 
 
class Recipe(db.Model):
 
    __tablename__ = "recipes"
 
    id = db.Column(db.Integer, primary_key=True)
    recipe_title = db.Column(db.String, nullable=False)
    recipe_description = db.Column(db.String, nullable=False)
    is_public = db.Column(db.Boolean, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
 
    def __init__(self, title, description, user_id, is_public):
        self.recipe_title = title
        self.recipe_description = description
        self.is_public = is_public
        self.user_id = user_id
 
    def __repr__(self):
        return '<title {}'.format(self.name)
        
        
class User(db.Model):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    email_confirmed_on = db.Column(db.DateTime, nullable=True)
    registered_on = db.Column(db.DateTime, nullable=True)
    last_logged_in = db.Column(db.DateTime, nullable=True)
    current_logged_in = db.Column(db.DateTime, nullable=True)
    role = db.Column(db.String, default='user')
    recipes = db.relationship('Recipe', backref='user', lazy='dynamic')

    def __init__(self, email, plaintext_password, email_confirmation_sent_on=None, role='user'):
        self.email = email
        self.password = plaintext_password
        self.email_confirmation_sent_on = email_confirmation_sent_on
        self.registered_on = datetime.now()
        self.current_logged_in = datetime.now()
        self.role = role
        
        

        
           