create environment              : mkvirtualenv --python=$path_to_python_exe $envt_name
list environments               : lsvirtualenv
delete environment              : rmvirtualenv $envt_name
exit environment                : deactivate
activate and enter environment  : workon $envt_name
------------------------------------------------------------------
About postgres:
Enter docker:                   docker exec -it $id /bin/bash
                                docker exec -it python-rest-pg-db /bin/bash
Enter psql for database:        docker exec -it $id psql -d python-rest-pg-db -U admin
                                docker exec -it python-rest-pg-db psql -d python-rest-pg-db -p 5432 -h 127.0.0.1 -U admin

List of databases               \l
Enter database                  \c database-pg
List tables in database         \dt

PGDATA directory                PGDATA: /var/lib/postgresql/data 

Having persistent data for postgresql
    Create a volume:
        docker volume create --name postgresql-volume -d local
    Run:
        docker-compose -f docker-compose-pers.yml up -d --build
------------------------------------------------------------------
Running in local native mode
  on parent folder
  SET FLASK_CFG=flask.native.cfg
  SET FLASK_APP=./run.py
  flask run --host=0.0.0.0
  
  FLASK_CFG enable to choose flask.*.cfg configuration file (for SQLALCHEMY_DATABASE_URI especially)
           
        